###########  SJ CUSTOM ##################

TERM=xterm
# User specific aliases and functions
#export PS1='\[\e[0;33m\][\t \u@\[\e[0;35m\]\h \[\e[0;33m\]\W]\$\[\e[m\] '
alias stamp='echo $(date +"%Y_%m_%d_%H%M_%S")'
alias apt='sudo apt'
alias cls='clear'
alias sdiff='sdiff -iWBbsd '
alias killme='ps -o pid= -u $USER |xargs kill -9'
alias pstree='pstree -uhalp'
alias mypstree='pstree $USER'
alias watchme=' while sleep 5; do mypstree; echo -e "------------------------- END  -----------------------"; done'
alias today='tp=$HOME/work/`TZ=EST date +%Y"/"%b"/"%d`; if ! [ -e $tp ]; then mkdir -p $tp; fi; chmod -R a+rX,go-w $tp; cd $tp'
alias now='date +"%Y%m%d"'
alias mydate='echo -e $(date +"%Y%m%d")'
alias yesterday='date +"%Y%m%d" -d "yesterday"'
alias ll='ls -lah'
alias l='ls -lh'
alias genpass='date +%s | sha256sum | base64 | head -c 16 ; echo'
alias wget='wget --no-check-certificate'
alias findzero='find . -size 0 -type f -delete'
alias tips='cat $HOME/tips.txt'
alias tipsbak='cp $HOME/tips.txt $HOME/tips.txt.$(date +"%Y_%m_%d_%H%M_%S")'
alias setgrep='export GREP_OPTIONS="--color=always"'
alias unsetgrep='unset GREP_OPTIONS'
alias w='watch  w'
alias p='mypstree'
alias rmcolor='sed "s,\x1B\[[0-9;]*[a-zA-Z],,g"'
alias nls='ss -anotp|grep LISTEN'
alias ip='ip -br'
function myscreen { if $(screen -ls|grep -q pts); then screen -Ax; else screen -AR; fi; }
function lst { ls -lathr ${1} | tail; }
function bu() { cp $1 $1.$(date +"%Y_%m_%d_%I_%M_%p"); }
if [ -x /usr/bin/dircolors ]; then
   test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)";
fi
export -f myscreen
export -f lst
export -f bu

#export DISPLAY=$(ip route | awk '{print $3; exit}'):0
#export DISPLAY=localhost:10.0
#export LIBGL_ALWAYS_INDIRECT=1
#export XCURSOR_SIZE=16

changeip() {
    if [ "$#" -ne 2 ]; then
        echo "Usage: changeip <old_ip> <new_ip>"
        return 1
    fi

    local old_ip=$1
    local new_ip=$2

    # Replace in file contents
    grep -rl "$old_ip" . | xargs sed -i "s/$old_ip/$new_ip/g"

    # Replace in file names
    for file in *"$old_ip"*; do
        mv "$file" "$(echo "$file" | sed "s/$old_ip/$new_ip/g")"
    done

    echo "IP address replacement completed: $old_ip -> $new_ip"
}

alias hc='rm -f ~/.zsh_history && kill -9 $$'

# Created by `pipx` on 2024-12-29 14:40:14
export PATH="$PATH:/root/.local/bin"

############ SJ END CUSTOM #####################
